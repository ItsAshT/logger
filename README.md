# Logger

This logger is based off Log from [_Charm](https://github.com/charmbracelet/log), I liked the way it displays information to the admins.

### Install
|package manager|install code                  |
|:---------------:|:--------------------------:|
|npm              |npm install itsasht-logger  |
|bun              |bun add itsasht-logger      |

Once you have installed the package you can use it by adding the following code: 

```javascript 
const { log } = require('itsasht-logger'); //CommonJS
import { log } from 'itsasht-logger'; //ESM
```

### log.info
```javascript
log.info('This is a basic log message');
```
```javascript
log.info('This is a more advanced log message', ["advanced", true], ["INFO", "This something different"])
```
```javascript
log.new("DEBUG", "aqua", "This is a debug message", ["debug", "test debug messages"])
```

```javascript
log.newRGB("Test", 107, 11, 224, "this is the color purple", ["test", "purple testing"])
```

The other functions are `log.warning` and `log.error` used in the same way as info shown above.

### log.doc
```javascript
log.doc("npm", "https://www.npmjs.com/package/itsasht-logger?activeTab=readme")
```
<img src="https://cdn.discordapp.com/attachments/692427489678458970/1203212396727107594/image.png?ex=65d045dd&is=65bdd0dd&hm=702dae61028c2c55be9bad98adc99e9ce73105bc8fe75c549d172f68651294aa&">