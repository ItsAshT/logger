const resetColor = `\x1b[0m`;

const ansiRGB = (r: number, g: number, b: number, text: string) =>
  `\x1b[38;2;${r};${g};${b}m${text + resetColor}`;

//Formatting
const format = {
  bold: (text: string) => `\x1b[1m${text + resetColor}`,
  italic: (text: string) => `\x1b[3m${text + resetColor}`,
  underline: (text: string) => `\x1b[4m${text + resetColor}`,
};
//Colors
const colors = {
  reset: (text: string) => `${text + resetColor}`,
  red: (text: string) => `\x1b[38;5;1m${text + resetColor}`, //Error
  aqua: (text: string) => `\x1b[38;5;86m${text + resetColor}`, //Information
  grey: (text: string) => `\x1b[38;5;244m${text + resetColor}`,
  white: (text: string) => `\x1b[38;5;255m${text + resetColor}`,
  orange: (text: string) => `\x1b[38;5;208m${text + resetColor}`, //Warning
  pink: (text: string) => `\x1b[38;5;169m${text + resetColor}`,
};

//Current Time
function currentTime() {
  let tz_offset = new Date().getTimezoneOffset() * 60000;
  return new Date(Date.now() - tz_offset)
    .toISOString()
    .slice(0, -5)
    .replace("T", " ");
}

function logging(
  logType: string,
  color: keyof typeof colors,
  message: string,
  ...keyvals: any[]
) {
  if (typeof message !== "string") return;

  return console.log(
    `${colors.white(currentTime())} ${format.bold(
      colors[color](logType)
    )} ${colors.white(message)} ${keyvals
      .map((keyval) => {
        if (typeof keyval[1] === "string") {
          return `${colors[color](keyval[0])}${colors.grey(
            "="
          )}\"${colors.white(keyval[1])}\"`;
        }
        return `${colors[color](keyval[0])}${colors.grey("=")}${colors.white(
          keyval[1]
        )}`;
      })
      .join(" ")}`
  );
}

class Logger {
  info(message: string, ...keyvals: any[]) {
    return logging("INFO", "aqua", message, ...keyvals);
  }

  error(message: string, ...keyvals: any[]) {
    return logging("ERROR", "red", message, ...keyvals);
  }

  warning(message: string, ...keyvals: any[]) {
    return logging("WARN", "orange", message, ...keyvals);
  }

  doc(message: string, url: string) {
    return logging("DOCS", "pink", message, ["url", url]);
  }

  new(
    prefix: string,
    color: keyof typeof colors,
    message: string,
    ...keyvals: any[]
  ) {
    return logging(prefix, color, message, ...keyvals);
  }

  newRGB(
    prefix: string,
    r: number,
    g: number,
    b: number,
    message: string,
    ...keyvals: any[]
  ) {
    return console.log(
      `${colors.white(currentTime())} ${format.bold(
        ansiRGB(r, g, b, prefix)
      )} ${colors.white(message)} ${keyvals
        .map((keyval) => {
          if (typeof keyval[1] === "string") {
            return `${ansiRGB(r, g, b, keyval[0])}${colors.grey(
              "="
            )}\"${colors.white(keyval[1])}\"`;
          }
          return `${ansiRGB(r, g, b, keyval[0])}${colors.grey(
            "="
          )}${colors.white(keyval[1])}`;
        })
        .join(" ")}`
    );
  }
}

export const log = new Logger();
